package com.agero.ncc.dispatcher.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.firestore.FirestoreJobData;
import com.agero.ncc.fragments.AssignDriverFragment;
import com.agero.ncc.fragments.BaseFragment;
import com.agero.ncc.fragments.BaseJobDetailsFragment;
import com.agero.ncc.fragments.EditServiceFragment;
import com.agero.ncc.fragments.EditTowDisablementFragment;
import com.agero.ncc.fragments.JobStatusFragment;
import com.agero.ncc.fragments.TowDestinationFragment;
import com.agero.ncc.fragments.UpdateETAFragment;
import com.agero.ncc.model.DisablementLocation;
import com.agero.ncc.model.Status;
import com.agero.ncc.model.TowLocation;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.JobActionsBottomSheetDialog;
import com.agero.ncc.views.TowBottomSheetDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import static com.agero.ncc.utils.NccConstants.JOB_STATUS_ASSIGNED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_DESTINATION_ARRIVAL;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_EN_ROUTE;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_JOB_COMPLETED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_ON_SCENE;


public class JobDetailsFragment extends BaseJobDetailsFragment implements TowBottomSheetDialog.TowBottomClickLisener, BaseFragment.OnSingleChoiceListener {
    String lat,lang;
    private long oldRetryTime = 0;
    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if( isAdded()) {
                if (dataSnapshot.getValue() != null) {
                    updateDriverJobDetails(dataSnapshot);
                    hideProgress();
                } else {
                    hideProgress();
                    if (!getResources().getBoolean(R.bool.isTablet)) {
                        mHomeActivity.popBackStackImmediate();
                    } else {
                        if (!mHomeActivity.isFinishing() && isAdded()) {
                            mHomeActivity.popAllBackstack();
                            mHomeActivity.push(ActiveJobsFragment.newInstance());
                        }
                    }
                }
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Dispatcher Job Details Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    public JobDetailsFragment() {
        // Intentionally empty
    }

    public static JobDetailsFragment newInstance(String sectionNumber, boolean isFromHistory, boolean isFromSearch) {
        JobDetailsFragment fragment = new JobDetailsFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, sectionNumber);
        args.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isFromHistory);
        args.putBoolean(NccConstants.IS_FROM_SEARCH, isFromSearch);
        fragment.setArguments(args);
        return fragment;
    }

    public static JobDetailsFragment newInstance(String sectionNumber, boolean isItFromHistory) {
        JobDetailsFragment fragment = new JobDetailsFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, sectionNumber);
        args.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isItFromHistory);
        args.putBoolean(NccConstants.IS_FROM_SEARCH, false);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mDispatchNumber = bundle.getString(NccConstants.DISPATCH_REQUEST_NUMBER);
            isItFromHistory = bundle.getBoolean(NccConstants.IS_IT_FROM_HISTORY);
            isFromSearch = bundle.getBoolean(NccConstants.IS_FROM_SEARCH);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getResources().getBoolean(R.bool.isTablet)) {
            mEditor.putBoolean(NccConstants.SHOW_ON_OFF_DUTY, false).commit();
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mDispatchNumber = bundle.getString(NccConstants.DISPATCH_REQUEST_NUMBER);
            isItFromHistory = bundle.getBoolean(NccConstants.IS_IT_FROM_HISTORY);
        }
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        if (isItFromHistory) {
            myRef = database.getReference("InActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber);
            //myRef.keepSynced(true);
        } else {
            myRef = database.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber);
            //myRef.keepSynced(true);
        }
        if (FirestoreJobData.getInStance().isFirestore()) {
            mCurrentJob = FirestoreJobData.getInStance().getJobDetail();
            loadJob();
        } else {
            getDataFromFirebase();
        }
        if (isPermissionDenied) {
            isPermissionDenied = false;
            showPermissionDeniedDialog();
        }

        if(getFragmentManager()!=null && getActivity()!=null) {
            if(isAdded() && isVisible() && getUserVisibleHint()) {
                loadMap();

            }
        }

    }

    private void getDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {

            if(!getResources().getBoolean(R.bool.isTablet)){
                showProgress();
            }

            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if(isAdded()) {
                        myRef.addValueEventListener(valueEventListener);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });

        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if(myRef != null && valueEventListener != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewClicked(View view) {
        super.onViewClicked(view);
        String eventAction = "";
        String eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
        switch (view.getId()) {
            case R.id.text_service_status_description:
                if(isItFromHistory){
                    eventAction = NccConstants.FirebaseEventAction.VIEW_TIME_LINE;
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_HISTORY_DETAILS;
                } else {
                    eventAction = NccConstants.FirebaseEventAction.MODIFY_STATUS;
                }

                if(mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchId())) {
                    mHomeActivity.push(JobStatusFragment.newInstance(mDispatchNumber, isItFromHistory, Utils.isTow(mCurrentJob.getServiceTypeCode()),isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()),mCurrentJob.getDisablementLocation().getTimeZone(), mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(),mCurrentJob), getString(R.string.title_job_modify_status));
                }
                mHomeActivity.firebaseLogEvent(eventCategory,eventAction);

                break;
            case R.id.text_service_driver_description:
                mHomeActivity.push(AssignDriverFragment.newInstance(mDispatchNumber,mCurrentJob), getString(R.string.title_assign_driver));
                eventAction = NccConstants.FirebaseEventAction.ASSIGN_DRIVER;
                mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                break;
            case R.id.button_start_job:
                Button but = (Button)view;
                if(but.getText().toString().trim().equalsIgnoreCase(getString(R.string.assigned_button_job_complete))){
                    ArrayList<Status>  statusHistory = mCurrentJob.getStatusHistory();
                    if(statusHistory == null){
                        statusHistory = new ArrayList<>();
                    }

                    statusHistory.add(mCurrentJob.getCurrentStatus());
                    updateStatusAPI(JOB_STATUS_JOB_COMPLETED, DateTimeUtils.getUtcTimeFormat(),mCurrentJob.getDispatchId(), statusHistory,mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(),null);
                }else {
                    ArrayList<Status>  statusHistory = mCurrentJob.getStatusHistory();
                    if(statusHistory == null){
                        statusHistory = new ArrayList<>();
                    }

                    statusHistory.add(mCurrentJob.getCurrentStatus());
                    if (JOB_STATUS_ASSIGNED.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        eventAction = NccConstants.FirebaseEventAction.START_JOB;
                        mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                        updateStatusAPI(JOB_STATUS_EN_ROUTE,DateTimeUtils.getUtcTimeFormat(),mCurrentJob.getDispatchId(), statusHistory,mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(),null);
                    } else if (JOB_STATUS_DESTINATION_ARRIVAL.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        updateStatusAPI(JOB_STATUS_JOB_COMPLETED,DateTimeUtils.getUtcTimeFormat(),mCurrentJob.getDispatchId(), statusHistory,mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(),null);
                    } else if (JOB_STATUS_ON_SCENE.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        updateStatusAPI(JOB_STATUS_JOB_COMPLETED,DateTimeUtils.getUtcTimeFormat(),mCurrentJob.getDispatchId(), statusHistory,mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(),null);
                    }
                }

                /*Button but = (Button)view;
                if(but.getText().toString().trim().equalsIgnoreCase(getString(R.string.assigned_button_job_complete))){
                    updateCurrentStatus(JOB_STATUS_JOB_COMPLETED,mCurrentJob.getDispatchId(), myRef.child("currentStatus"), myRef.child("statusHistory"));
                }else {
                    if (JOB_STATUS_ASSIGNED.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        eventAction = NccConstants.FirebaseEventAction.START_JOB;
                        mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                        updateCurrentStatus(JOB_STATUS_EN_ROUTE,mCurrentJob.getDispatchId(), myRef.child("currentStatus"), myRef.child("statusHistory"));
                    } else if (JOB_STATUS_DESTINATION_ARRIVAL.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        updateCurrentStatus(JOB_STATUS_JOB_COMPLETED,mCurrentJob.getDispatchId(), myRef.child("currentStatus"), myRef.child("statusHistory"));
                    } else if (JOB_STATUS_ON_SCENE.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        updateCurrentStatus(JOB_STATUS_JOB_COMPLETED,mCurrentJob.getDispatchId(), myRef.child("currentStatus"), myRef.child("statusHistory"));
                    }
                }*/
                break;
        }
    }

    @Override
    public void onChoiceClicked(int position, int options) {
        if(isAdded()) {
            String reportCode = "", reportReason = "";
            switch (options) {
                case NccConstants.DISPATCHER_REPORT_GOA:
                    reportCode = getResources().getStringArray(R.array.array_report_goa_reason_codes)[position];
                    //updateStatus(JOB_STATUS_GOA, reportCode, "Report Goa");
                    updateGOAStatusAPI(reportCode, DateTimeUtils.getUtcTimeFormat(), mCurrentJob.getDispatchId(), mCurrentJob.getStatusHistory(), mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), new StatusUpdateListener() {
                        @Override
                        public void onSuccess() {
                            if (isAdded() && !getResources().getBoolean(R.bool.isTablet)) {
                                mHomeActivity.popAllBackstack();
                                mHomeActivity.mBottomBar.setCurrentItem(0);
                            }
                        }

                        @Override
                        public void onFailure() {

                        }
                    });
                    break;
                case NccConstants.DISPATCHER_REPORT_GOA_ASM:

                    break;
                case NccConstants.DISPATCHER_REPORT_GOA_VRM:

                    break;
                case NccConstants.DISPATCHER_SERVICE_UN_SUCCESSFUL:
                    reportCode = getResources().getStringArray(R.array.array_service_unsuccessful_reason_code)[position];
                    updateStatus(NccConstants.JOB_STATUS_UNSUCCESSFUL, reportCode, "Report Service Unsuccessful");
                    break;
                case NccConstants.DISPATCHER_CANCEL_SERVICE:
                    if (position == 0) {
                        String[] provider = getResources().getStringArray(R.array.array_provider_cancel_service);
                        displaySingleChoiceAlertDialog(getResources().getString(R.string.title_provider_cancel_service), provider, getResources().getString(R.string.cancel_service_second_pager), getResources().getString(R.string.ok), JobDetailsFragment.this::onChoiceClicked, NccConstants.DISPATCHER_CANCEL_SERVICE_PROVIDER);
                    } else {
                        String[] customer = getResources().getStringArray(R.array.array_customer_cancel_service);
                        displaySingleChoiceAlertDialog(getResources().getString(R.string.title_customer_cancel_service), customer, getResources().getString(R.string.cancel_service_second_pager), getResources().getString(R.string.ok), JobDetailsFragment.this::onChoiceClicked, NccConstants.DISPATCHER_CANCEL_SERVICE_CUSTOMER);
                    }
                    break;
                case NccConstants.DISPATCHER_CANCEL_SERVICE_PROVIDER:
                    reportCode = getResources().getStringArray(R.array.array_provider_cancel_service_reason_code)[position];
                    //updateStatus(NccConstants.JOB_STATUS_CANCELLED, reportCode, "Report Cancel", "PARTNER_CANCEL_REQUEST");
                    updateCancelStatusAPI(reportCode, DateTimeUtils.getUtcTimeFormat(), mCurrentJob.getDispatchId(), mCurrentJob.getStatusHistory(), mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), new StatusUpdateListener() {
                        @Override
                        public void onSuccess() {
                            if (isAdded() && !getResources().getBoolean(R.bool.isTablet)) {
                                mHomeActivity.popAllBackstack();
                                mHomeActivity.mBottomBar.setCurrentItem(0);
                            }
                        }

                        @Override
                        public void onFailure() {

                        }
                    });
                    break;
                case NccConstants.DISPATCHER_CANCEL_SERVICE_CUSTOMER:
                    reportCode = getResources().getStringArray(R.array.array_customer_cancel_service_reason_code)[position];
                    //updateStatus(NccConstants.JOB_STATUS_CANCELLED, reportCode, "Report Cancel", "PARTNER_CANCEL_REQUEST_CUSTOMER");
                    updateCancelStatusAPI(reportCode, DateTimeUtils.getUtcTimeFormat(), mCurrentJob.getDispatchId(), mCurrentJob.getStatusHistory(), mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), new StatusUpdateListener() {
                        @Override
                        public void onSuccess() {
                            if (isAdded() && !getResources().getBoolean(R.bool.isTablet)) {
                                mHomeActivity.popAllBackstack();
                                mHomeActivity.mBottomBar.setCurrentItem(0);
                            }
                        }

                        @Override
                        public void onFailure() {

                        }
                    });
                    break;
            }
        }
    }

    private void showDispatcherJobOptionsBottomSheet() {
        final JobActionsBottomSheetDialog dispatcherBottomSheetDialog = JobActionsBottomSheetDialog.getInstance(Utils.isTow(mCurrentJob.getServiceTypeCode()), isUpdateEta(),mCurrentJob.getCurrentStatus().getStatusCode());
        dispatcherBottomSheetDialog.show(getFragmentManager(), "Job Options");
        dispatcherBottomSheetDialog.setDispatcherJobOptionsDialogListener(new JobActionsBottomSheetDialog.DispatcherJobOptionsDialogListener() {
            @Override
            public void onGeofenceJobsClick() {
                dispatcherBottomSheetDialog.dismiss();
                if (isAdded()) {
                    showGeofenceArray();
                }
            }

            @Override
            public void onModifyStatusClick() {
                dispatcherBottomSheetDialog.dismiss();
                if(isAdded() && mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchId())) {
                    mHomeActivity.push(JobStatusFragment.newInstance(mDispatchNumber, isItFromHistory, Utils.isTow(mCurrentJob.getServiceTypeCode()),isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()),mCurrentJob.getDisablementLocation().getTimeZone(), mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(),mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(),mCurrentJob), getString(R.string.title_job_modify_status));
                }
            }

            @Override
            public void onUpdateEtaClick() {
                dispatcherBottomSheetDialog.dismiss();
                showUpdateETADialog();
            }

            @Override
            public void onAssignDriverClick() {
                dispatcherBottomSheetDialog.dismiss();
                if(isAdded()) {
                    mHomeActivity.push(AssignDriverFragment.newInstance(mDispatchNumber,mCurrentJob), getString(R.string.title_assign_driver));
                }
            }

            @Override
            public void onReportGoaClick() {
                dispatcherBottomSheetDialog.dismiss();
                if(isAdded()) {
                    String[] reportGoa = getResources().getStringArray(R.array.array_report_goa);
                    displaySingleChoiceAlertDialog(getResources().getString(R.string.title_report_goa), reportGoa, "", getResources().getString(R.string.ok), JobDetailsFragment.this::onChoiceClicked, NccConstants.DISPATCHER_REPORT_GOA);
                }

            }

            @Override
            public void onServiceUnsuccessClick() {
                dispatcherBottomSheetDialog.dismiss();
//                mHomeActivity.push(ServiceUnsuccessfulFragment.newInstance(), getString(R.string.title_service_unsuccessful));
                if(isAdded()) {
                    String[] serviceUnsuccessful = getResources().getStringArray(R.array.array_service_unsuccessful_reason);
                    displaySingleChoiceAlertDialog(getResources().getString(R.string.title_service_unsuccessful), serviceUnsuccessful, "", getResources().getString(R.string.ok), JobDetailsFragment.this::onChoiceClicked, NccConstants.DISPATCHER_SERVICE_UN_SUCCESSFUL);
                }
            }

            @Override
            public void onCancelServiceClick() {
                dispatcherBottomSheetDialog.dismiss();
                if(isAdded()) {
                    String[] cancelService = getResources().getStringArray(R.array.array_cancel_service);
                    displaySingleChoiceAlertDialog(getResources().getString(R.string.title_cancel_service), cancelService, getResources().getString(R.string.cancel_service_first_pager), getResources().getString(R.string.label_continue), JobDetailsFragment.this::onChoiceClicked, NccConstants.DISPATCHER_CANCEL_SERVICE);
                }
            }

            @Override
            public void onEditServiceClick() {
                dispatcherBottomSheetDialog.dismiss();
                if(isAdded()) {
                    mHomeActivity.push(EditServiceFragment.newInstance(mUserProfile.getFirstName(), mDispatchNumber), getString(R.string.edit_service_label));
                }
            }

            @Override
            public void onEditTowDestination() {
                dispatcherBottomSheetDialog.dismiss();
                if(isAdded()) {
//                    showTowBottomSheet();
                    editTowDisablementLocation(true);
                }
            }

            @Override
            public void onEditDisablementLocation() {
                dispatcherBottomSheetDialog.dismiss();
                if(isAdded()) {
//                   showDisablementBottomSheet();
                    editTowDisablementLocation(false);
                }
            }
        });
    }

    private void showTowBottomSheet() {
        final TowBottomSheetDialog towBottomSheetDialog = TowBottomSheetDialog.newInstance(true, new Gson().toJson(mCurrentJob));
        towBottomSheetDialog.show(getFragmentManager(), "Edit Tow Destination");
        towBottomSheetDialog.setTowBottomClickLisener(this);
    }
    private void showDisablementBottomSheet(){
        final TowBottomSheetDialog towBottomSheetDialog = TowBottomSheetDialog.newInstance(false,new Gson().toJson(mCurrentJob));
        towBottomSheetDialog.show(getFragmentManager(), "Edit Disabalement Location");
        towBottomSheetDialog.setTowBottomClickLisener(this);
    }

    public void showUpdateETADialog() {
        if (isUpdateEta() && isAdded()) {
            if(mUserProfile != null){
                mHomeActivity.push(UpdateETAFragment.newInstance(
                        mUserProfile.getFirstName() == null ? " " : mUserProfile.getFirstName(), mDispatchNumber,mCurrentJob.getDisablementLocation().getTimeZone()), getString(R.string.alert_label_update_eta));
            } else{
                mHomeActivity.push(UpdateETAFragment.newInstance(" " , mDispatchNumber,mCurrentJob.getDisablementLocation().getTimeZone()), getString(R.string.alert_label_update_eta));
            }

        }
    }

    @Override
    public void onOptions() {
        if(mCurrentJob != null && isAdded() && !mHomeActivity.isFinishing()) {
            try{
                showDispatcherJobOptionsBottomSheet();
            }catch (java.lang.IllegalStateException e){
            }
        }
    }

    public void getDirection(boolean isTowDirection){
        if(isTowDirection){
            lat=mCurrentJob.getTowLocation().getLatitude().toString();
            lang=mCurrentJob.getTowLocation().getLongitude().toString();
        }else{
            lat=mCurrentJob.getDisablementLocation().getLatitude().toString();
            lang=mCurrentJob.getDisablementLocation().getLongitude().toString();
        }
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:<lat>,<long>?q=<"+lat+">,<"+lang+">"));
        startActivity(intent);
    }

    public void editTowDisablementLocation(boolean isTow){
        if(isTow){
            TowLocation towDestination = mCurrentJob.getTowLocation();
            if(towDestination.getStatus().equalsIgnoreCase("REQUEST")){
                mUserError.title = getString(R.string.update_address_pending_title);
                mUserError.message = getString(R.string.update_address_pending_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            }else {
                if(mUserProfile != null){
                    mHomeActivity.push(TowDestinationFragment.newInstance(mDispatchNumber, mUserProfile.getFirstName()), getString(R.string.title_edit_tow));
                }

            }
        }else{
            DisablementLocation disablementLocation = mCurrentJob.getDisablementLocation();
            if(disablementLocation.getStatus().equalsIgnoreCase("REQUEST")){
                mUserError.title = getString(R.string.update_address_pending_title);
                mUserError.message = getString(R.string.update_address_pending_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            }else {
                if(mUserProfile != null) {
                    mHomeActivity.push(EditTowDisablementFragment.newInstance(true, mDispatchNumber, mUserProfile.getFirstName()), getString(R.string.title_edit_tow));
                }
            }


        }

    }

    @Override
    public void isGetDirectionClick(boolean isTowDirection) {
        if(isAdded()) {
            getDirection(isTowDirection);
        }
    }

    @Override
    public void isTowDisablemtClick(boolean isTow) {
        if(isAdded()) {
            editTowDisablementLocation(isTow);
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if(myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if(myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if(myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
