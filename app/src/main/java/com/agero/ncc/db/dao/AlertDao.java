package com.agero.ncc.db.dao;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.agero.ncc.db.entity.Alert;

import java.util.Date;
import java.util.List;

@Dao
public interface AlertDao {

    @Query("SELECT * FROM alert ORDER BY updated_time DESC")
    LiveData<List<Alert>> getAlerts();

    @Query("SELECT * FROM alert WHERE alertId == :alertId")
    Alert findByAlertId(long alertId);

    @Query("DELETE FROM alert WHERE updated_time BETWEEN :startTime  AND :endTime")
    void deleteByCurrentDate(Date startTime,Date endTime);

    /*@Query("DELETE FROM alert WHERE updated_time <= :date")
    void deleteByOldDate(Date date);*/

    @Query("DELETE FROM alert WHERE updated_time <= :startTime OR updated_time >= :endTime")
    void deleteByOldDate(Date startTime,Date endTime);

    @Query("DELETE FROM alert")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Alert... alerts);

    @Delete
    void delete(Alert alert);

    @Query("SELECT COUNT(alertId) FROM alert WHERE read = 0")
    int getUnreadCount();


}
