package com.agero.ncc.driver.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.CreateProfileActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.firestore.FirestoreJobData;
import com.agero.ncc.fragments.BaseJobDetailsFragment;
import com.agero.ncc.fragments.EditTowDisablementFragment;
import com.agero.ncc.fragments.JobStatusFragment;
import com.agero.ncc.fragments.NotesFragment;
import com.agero.ncc.fragments.TowDestinationFragment;
import com.agero.ncc.fragments.UpdateETAFragment;
import com.agero.ncc.model.DisablementLocation;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Status;
import com.agero.ncc.model.TowLocation;
import com.agero.ncc.services.SparkLocationUpdateService;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.views.JobActionsBottomSheetDialog;
import com.agero.ncc.views.TowBottomSheetDialog;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import static com.agero.ncc.utils.NccConstants.JOB_STATUS_ASSIGNED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_DECLINED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_DESTINATION_ARRIVAL;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_EN_ROUTE;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_EXPIRED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_JOB_COMPLETED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_OFFERED;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_ON_SCENE;
import static com.agero.ncc.utils.NccConstants.JOB_STATUS_REJECTED;

public class JobDetailsFragment extends BaseJobDetailsFragment implements HomeActivity.JobDetailToolbarLisener, TowBottomSheetDialog.TowBottomClickLisener {

    private long oldRetryTime = 0;
    Bundle bundle = new Bundle();
    String eventAction = "";
    String eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;

    public JobDetailsFragment() {
        // Intentionally empty
    }

    public static JobDetailsFragment newInstance(String sectionNumber, boolean isFromHistory, boolean isFromSearch) {
        JobDetailsFragment fragment = new JobDetailsFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, sectionNumber);
        args.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isFromHistory);
        args.putBoolean(NccConstants.IS_FROM_SEARCH, isFromSearch);
        fragment.setArguments(args);
        return fragment;
    }

    public static JobDetailsFragment newInstance(String sectionNumber, boolean isFromHistory) {
        JobDetailsFragment fragment = new JobDetailsFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, sectionNumber);
        args.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isFromHistory);
        args.putBoolean(NccConstants.IS_FROM_SEARCH, false);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mDispatchNumber = bundle.getString(NccConstants.DISPATCH_REQUEST_NUMBER);
            isItFromHistory = bundle.getBoolean(NccConstants.IS_IT_FROM_HISTORY);
            isFromSearch = bundle.getBoolean(NccConstants.IS_FROM_SEARCH);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (!getResources().getBoolean(R.bool.isTablet)) {
            mEditor.putBoolean(NccConstants.SHOW_ON_OFF_DUTY, false).commit();
        }
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mDispatchNumber = bundle.getString(NccConstants.DISPATCH_REQUEST_NUMBER);
            isItFromHistory = bundle.getBoolean(NccConstants.IS_IT_FROM_HISTORY);
        }
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        if (isItFromHistory) {
            myRef = database.getReference("InActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber);
            // myRef.keepSynced(true);
        } else {
            myRef = database.getReference("ActiveJobs/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatchNumber);
            myRef.keepSynced(true);
        }
        if (getFragmentManager() != null && getActivity() != null) {
            if (isAdded() && isVisible() && getUserVisibleHint()) {
                loadMap();
            }
        }
        if (FirestoreJobData.getInStance().isFirestore()) {
            mCurrentJob = FirestoreJobData.getInStance().getJobDetail();
            loadJob();
        } else {
            getDataFromFirebase();
        }
        if (isPermissionDenied) {
            isPermissionDenied = false;
            showPermissionDeniedDialog();
        }

    }

    private void getDataFromFirebase() {
        if (Utils.isNetworkAvailable()) {

            if (!getResources().getBoolean(R.bool.isTablet)) {
                showProgress();
            }

            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        myRef.addValueEventListener(valueEventListener);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = "";
            mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            if (myRef != null && valueEventListener != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onViewClicked(View view) {
        super.onViewClicked(view);
        switch (view.getId()) {
            case R.id.button_start_job:
                Button but = (Button) view;
                if (but.getText().toString().trim().equalsIgnoreCase(getString(R.string.assigned_button_job_complete))) {
                    ArrayList<Status> statusHistory = mCurrentJob.getStatusHistory();
                    if (statusHistory == null) {
                        statusHistory = new ArrayList<>();
                    }

                    statusHistory.add(mCurrentJob.getCurrentStatus());
                    updateStatusAPI(JOB_STATUS_JOB_COMPLETED, DateTimeUtils.getUtcTimeFormat(), mCurrentJob.getDispatchId(), statusHistory, mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), null);
                    if (NccConstants.IS_GEOFENCE_ENABLED) {
                        SparkLocationUpdateService.checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mCurrentJob);
                    }
                } else {
                    ArrayList<Status> statusHistory = mCurrentJob.getStatusHistory();
                    if (statusHistory == null) {
                        statusHistory = new ArrayList<>();
                    }

                    statusHistory.add(mCurrentJob.getCurrentStatus());

                    if (JOB_STATUS_ASSIGNED.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        eventAction = NccConstants.FirebaseEventAction.START_JOB;
                        mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
                        if (!TextUtils.isEmpty(mCurrentJob.getDispatchAssignedToId()) && mCurrentJob.getDispatchAssignedToId().equalsIgnoreCase(mPrefs.getString(NccConstants.SIGNIN_USER_ID, "")) && NccConstants.IS_GEOFENCE_ENABLED) {
                            SparkLocationUpdateService.checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mCurrentJob);
                        }
                        updateStatusAPI(JOB_STATUS_EN_ROUTE, DateTimeUtils.getUtcTimeFormat(), mCurrentJob.getDispatchId(), statusHistory, mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), null);
                    } else if (JOB_STATUS_DESTINATION_ARRIVAL.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        updateStatusAPI(JOB_STATUS_JOB_COMPLETED, DateTimeUtils.getUtcTimeFormat(), mCurrentJob.getDispatchId(), statusHistory, mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), null);
                    } else if (JOB_STATUS_ON_SCENE.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        updateStatusAPI(JOB_STATUS_JOB_COMPLETED, DateTimeUtils.getUtcTimeFormat(), mCurrentJob.getDispatchId(), statusHistory, mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), null);
                    }
                }

                /*Button but = (Button) view;
                if (but.getText().toString().trim().equalsIgnoreCase(getString(R.string.assigned_button_job_complete))) {
                    updateCurrentStatus(JOB_STATUS_JOB_COMPLETED, mCurrentJob.getDispatchId(), myRef.child("currentStatus"), myRef.child("statusHistory"));
                } else {
                    if (JOB_STATUS_ASSIGNED.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        eventAction = NccConstants.FirebaseEventAction.START_JOB;
                        mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
                        updateCurrentStatus(JOB_STATUS_EN_ROUTE, mCurrentJob.getDispatchId(), myRef.child("currentStatus"), myRef.child("statusHistory"));
                    } else if (JOB_STATUS_DESTINATION_ARRIVAL.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        updateCurrentStatus(JOB_STATUS_JOB_COMPLETED, mCurrentJob.getDispatchId(), myRef.child("currentStatus"), myRef.child("statusHistory"));
                    } else if (JOB_STATUS_ON_SCENE.equals(mCurrentJob.getCurrentStatus().getStatusCode())) {
                        updateCurrentStatus(JOB_STATUS_JOB_COMPLETED, mCurrentJob.getDispatchId(), myRef.child("currentStatus"), myRef.child("statusHistory"));
                    }
                }*/
                break;
            case R.id.text_service_status_description:
                if (isItFromHistory) {
                    eventAction = NccConstants.FirebaseEventAction.VIEW_TIME_LINE;
                    eventCategory = NccConstants.FirebaseEventCategory.JOB_HISTORY_DETAILS;
                } else {
                    eventAction = NccConstants.FirebaseEventAction.MODIFY_STATUS;
                }

                if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchId())) {
                    mHomeActivity.push(JobStatusFragment.newInstance(mDispatchNumber, isItFromHistory, Utils.isTow(mCurrentJob.getServiceTypeCode()), isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()), mCurrentJob.getDisablementLocation().getTimeZone(), mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), mCurrentJob), getString(R.string.title_job_modify_status));
                }
                mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
                break;
        }
    }

    @Override
    public void onCall() {
        if (Utils.isCallSupported(mHomeActivity)) {
            showCallSelectionBottomSheet();
        } else {
            mUserError.message = "Cannot make call";
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

  /*  @Override
    public void onNotes() {
        if (mCurrentJob != null && mCurrentJob.getCurrentStatus() != null && !TextUtils.isEmpty(mCurrentJob.getCurrentStatus().getStatusCode())) {
            if (isItFromHistory) {
                eventCategory = NccConstants.FirebaseEventCategory.JOB_HISTORY_DETAILS;
                eventAction = NccConstants.FirebaseEventAction.VIEW_NOTES;
            } else {
                eventCategory = NccConstants.FirebaseEventCategory.JOB_DETAILS;
                eventAction = NccConstants.FirebaseEventAction.VIEW_NOTES;
            }
            mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
            mHomeActivity.push(NotesFragment.newInstance(isItFromHistory, !(JOB_STATUS_OFFERED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_REJECTED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_EXPIRED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || JOB_STATUS_DECLINED.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode())), false, mDispatchNumber, mCurrentJob.getDispatchCreatedDate()), "");
        }
    }*/

    @Override
    public void onOptions() {
        if (mCurrentJob != null && isAdded()) {
            showDriverJobOptionsBottomSheet();
        }
    }

    private void showDriverJobOptionsBottomSheet() {
        final JobActionsBottomSheetDialog dispatcherBottomSheetDialog = JobActionsBottomSheetDialog.getInstance(Utils.isTow(mCurrentJob.getServiceTypeCode()), isUpdateEta(), mCurrentJob.getCurrentStatus().getStatusCode());
        dispatcherBottomSheetDialog.show(getFragmentManager(), "Job Options");
        dispatcherBottomSheetDialog.setDispatcherJobOptionsDialogListener(new JobActionsBottomSheetDialog.DispatcherJobOptionsDialogListener() {
            @Override
            public void onGeofenceJobsClick() {
                dispatcherBottomSheetDialog.dismiss();
                if (isAdded()) {
                    showGeofenceArray();
                }
            }

            @Override
            public void onModifyStatusClick() {
                dispatcherBottomSheetDialog.dismiss();
                if (isAdded() && mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchId())) {
                    mHomeActivity.push(JobStatusFragment.newInstance(mDispatchNumber, isItFromHistory, Utils.isTow(mCurrentJob.getServiceTypeCode()), isActiveJob(mCurrentJob.getCurrentStatus().getStatusCode()), mCurrentJob.getDisablementLocation().getTimeZone(), mCurrentJob.getDispatchAssignedToId(), mCurrentJob.getDispatchAssignedToName(), mCurrentJob.getDriverAssignedById(), mCurrentJob.getDriverAssignedByName(), mCurrentJob), getString(R.string.title_job_modify_status));
                }
            }

            @Override
            public void onUpdateEtaClick() {
                dispatcherBottomSheetDialog.dismiss();
                showUpdateETADialog();
            }

            @Override
            public void onAssignDriverClick() {
            }

            @Override
            public void onReportGoaClick() {
            }

            @Override
            public void onServiceUnsuccessClick() {
            }

            @Override
            public void onCancelServiceClick() {
            }

            @Override
            public void onEditServiceClick() {
            }

            @Override
            public void onEditTowDestination() {
                dispatcherBottomSheetDialog.dismiss();
                if (isAdded()) {
//                    showTowBottomSheet();
                    editTowDisablementLocation(true);
                }
            }

            @Override
            public void onEditDisablementLocation() {
                dispatcherBottomSheetDialog.dismiss();
                if (isAdded()) {
//                    showDisablementBottomSheet();
                    editTowDisablementLocation(false);
                }
            }
        });
    }

    private void showDisablementBottomSheet() {
        final TowBottomSheetDialog towBottomSheetDialog = TowBottomSheetDialog.newInstance(false, new Gson().toJson(mCurrentJob));
        towBottomSheetDialog.show(getFragmentManager(), "Edit Disabalement Location");
        towBottomSheetDialog.setTowBottomClickLisener(this);
    }

    public void showUpdateETADialog() {
        if (isUpdateEta() && isAdded()) {
            if (mUserProfile != null) {
                mHomeActivity.push(UpdateETAFragment.newInstance(
                        mUserProfile.getFirstName() == null ? " " : mUserProfile.getFirstName(), mDispatchNumber, mCurrentJob.getDisablementLocation().getTimeZone()), getString(R.string.alert_label_update_eta));
            } else {
                mHomeActivity.push(UpdateETAFragment.newInstance(" ", mDispatchNumber, mCurrentJob.getDisablementLocation().getTimeZone()), getString(R.string.alert_label_update_eta));
            }

        }
    }

    private void showTowBottomSheet() {
        final TowBottomSheetDialog towBottomSheetDialog = TowBottomSheetDialog.newInstance(true, new Gson().toJson(mCurrentJob));
        towBottomSheetDialog.show(getFragmentManager(), "Edit Tow Destination");
        towBottomSheetDialog.setTowBottomClickLisener(this);
    }

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if(isAdded()) {
                if (dataSnapshot.getValue() != null) {
                    updateDriverJobDetails(dataSnapshot);
                } else {
                    if (!getResources().getBoolean(R.bool.isTablet)) {
                        mHomeActivity.popBackStackImmediate();
                    } else {
                        mHomeActivity.popAllBackstack();
                        mHomeActivity.push(ActiveJobsFragment.newInstance());
                    }
                }
                hideProgress();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Driver Job details Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    @Override
    public void isGetDirectionClick(boolean isTowDirection) {
        if (isAdded()) {
            getDirection(isTowDirection);
        }
    }

    public void getDirection(boolean isTowDirection) {
        String lat;
        String lang;
        if (isTowDirection) {
            lat = mCurrentJob.getTowLocation().getLatitude().toString();
            lang = mCurrentJob.getTowLocation().getLongitude().toString();
        } else {
            lat = mCurrentJob.getDisablementLocation().getLatitude().toString();
            lang = mCurrentJob.getDisablementLocation().getLongitude().toString();
        }
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("geo:<lat>,<long>?q=<" + lat + ">,<" + lang + ">"));
        startActivity(intent);
    }


    @Override
    public void isTowDisablemtClick(boolean isTow) {
        if (isAdded()) {
            editTowDisablementLocation(isTow);
        }
    }

    public void editTowDisablementLocation(boolean isTow) {
        if (isTow) {
            TowLocation towDestination = mCurrentJob.getTowLocation();
            if (towDestination.getStatus().equalsIgnoreCase("REQUEST")) {
                mUserError.title = getString(R.string.update_address_pending_title);
                mUserError.message = getString(R.string.update_address_pending_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            } else {
                if (mUserProfile != null) {
                    mHomeActivity.push(TowDestinationFragment.newInstance(mDispatchNumber, mUserProfile.getFirstName()), getString(R.string.title_edit_tow));
                }

            }
        } else {
            DisablementLocation disablementLocation = mCurrentJob.getDisablementLocation();
            if (disablementLocation.getStatus().equalsIgnoreCase("REQUEST")) {
                mUserError.title = getString(R.string.update_address_pending_title);
                mUserError.message = getString(R.string.update_address_pending_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            } else {
                if (mUserProfile != null) {
                    mHomeActivity.push(EditTowDisablementFragment.newInstance(true, mDispatchNumber, mUserProfile.getFirstName()), getString(R.string.title_edit_tow));
                }
            }


        }

    }
}
