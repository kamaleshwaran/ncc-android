package com.agero.ncc.activities


import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.Toast

import com.agero.ncc.BuildConfig
import com.agero.ncc.R
import com.agero.ncc.app.NCCApplication
import com.agero.ncc.fragments.*
import com.agero.ncc.utils.*
import com.google.firebase.analytics.FirebaseAnalytics

import timber.log.Timber

/**
 * Created by sdevabhaktuni on 9/19/16.
 */

open class BaseActivity : AppCompatActivity() {

    var isActivityInForeground: Boolean = false
    var isActivityDestroyed: Boolean = false
    lateinit var mFirebaseAnalytics: FirebaseAnalytics
    var alertFragment: AlertDialogFragment? = null;
    var mHandler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (getResources().getBoolean(R.bool.isTablet)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        NCCApplication.getContext().component.inject(this)

        LogUtils.addLevel(LogLevel.ALL)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this@BaseActivity)
    }

    override fun onResume() {
        super.onResume()
        isActivityInForeground = true
    }

    override fun onPause() {
        super.onPause()
        isActivityInForeground = false
    }

    fun firebaseLogEvent(eventCategory:String, eventAction:String){
        if(!eventCategory.isEmpty() && !eventAction.isEmpty()) {
            val bundle = Bundle()
            bundle.putString("eventCategory", eventCategory)
            bundle.putString("eventAction", eventAction)
            Log.e("category-action", eventCategory + " - " + eventAction)
            mFirebaseAnalytics.logEvent(eventCategory + "_" + eventAction, bundle)
        }

    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        isActivityDestroyed = true
    }


    override fun startActivityForResult(intent: Intent, requestCode: Int) {
        try {
            super.startActivityForResult(intent, requestCode)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, responseCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, responseCode, intent)


    }

    fun showProgress() {
        val view: View = findViewById(R.id.progress_bar_lay)
        view.bringToFront()
        view.visibility = View.GONE
    }


    fun hideProgress() {
        val view: View = findViewById(R.id.progress_bar_lay)
        view.visibility = View.GONE
    }

    protected fun navigateBackIfNeeded(title: String): Boolean {
        val fragmentManager = supportFragmentManager
        val backCount = fragmentManager.backStackEntryCount
        if (backCount > 0) {
            for (i in 0 until backCount) {
                val entry = fragmentManager.getBackStackEntryAt(i)
                if (entry.name == title && !isFinishing) {
                    fragmentManager.popBackStackImmediate(entry.id, 0)
                    return true
                }
            }
        }
        return false
    }

    fun popBackStackImmediate() {
        val fragmentManager = supportFragmentManager
        try {
            if(!isFinishing){
                fragmentManager.popBackStack()
                fragmentManager.executePendingTransactions()
            }
        } catch (ile: IllegalStateException) {
        }


    }

    fun popAllBackstack() {
        val fragmentManager = supportFragmentManager
        val backCount = fragmentManager.backStackEntryCount
        if (backCount > 0) {
            try {
                if(!isFinishing){
                    fragmentManager.popBackStackImmediate(fragmentManager.getBackStackEntryAt(0).id,
                            FragmentManager.POP_BACK_STACK_INCLUSIVE)
                }
            } catch (ile: IllegalStateException) {
            }


        }
    }

    fun updateAnalyticsCurrentScreen(screenName: String) {
        mFirebaseAnalytics.setCurrentScreen(this, screenName, null)
    }

    fun tokenRefreshFailed() {
        if (this is HomeActivity) {
            val intent = Intent(this, WelcomeActivity::class.java)
            intent.putExtra(NccConstants.IS_REFRESH_TOKEN_FAILED, true)
            startActivity(intent)
            finish()
        }
    }

    fun push(fragment: Fragment, title: String?) {
        hideProgress()
        val fragmentManager = supportFragmentManager
        val tag = fragment.javaClass.canonicalName

        var id: Int = R.id.frame_home_screen_container


        fragmentManager.beginTransaction().replace(R.id.frame_home_screen_details_container, BaseFragment(), tag).commitAllowingStateLoss()

        findViewById<View>(R.id.view_separator).visibility = View.GONE

        if (resources.getBoolean(R.bool.isTablet) && (fragment is BaseJobDetailsFragment || fragment is EditProfileFragment || fragment is MyCompanyFragment || fragment is ZendeskHelpFragment || fragment is LegalFragment)) {
            findViewById<View>(R.id.view_separator).visibility = View.VISIBLE
            id = R.id.frame_home_screen_details_container
        }

        if (title != null) {
            try {
                fragmentManager.beginTransaction()
                        .replace(id, fragment, tag)
                        .addToBackStack(title)
                        .commit()
            } catch (ile: IllegalStateException) {
                fragmentManager.beginTransaction()
                        .replace(id, fragment, tag)
                        .addToBackStack(title)
                        .commitAllowingStateLoss()
            }

        } else {
            try {
                fragmentManager.beginTransaction().replace(id, fragment, tag).commit()
            } catch (ile: IllegalStateException) {
                fragmentManager.beginTransaction().replace(id, fragment, tag).commitAllowingStateLoss()
            }

        }

        if (id == R.id.frame_home_screen_details_container) {
            findViewById<FrameLayout>(R.id.frame_home_screen_details_container).visibility = View.VISIBLE
            setPercentage()
        } else {
            findViewById<FrameLayout>(R.id.frame_home_screen_details_container).visibility = View.GONE
        }

    }

    fun hideRightFrame(){
        findViewById<FrameLayout>(R.id.frame_home_screen_details_container).visibility = View.GONE
    }

    fun removeAllVehicleFragment() {

        for (fragment in supportFragmentManager.fragments) {
            if (fragment is VehicleReportFragment || fragment is VehicleReportTaggingFragment) {
                supportFragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss()
            }
        }
    }

    fun removeNewConversationFragment() {

        for (fragment in supportFragmentManager.fragments) {
            if (fragment is ChatNewConversationFragment && fragment.isVisible && fragment.isChatStarted) {
                popBackStackImmediate()
            }
        }
    }

    fun reloadChat() {

        for (fragment in supportFragmentManager.fragments) {
            if (fragment is ChatHistoryFragment && fragment.isVisible) {
                fragment.reloadData()
            }
        }
    }

    private fun setPercentage() {
        val view_instance = findViewById<View>(R.id.frame_home_screen_details_container) as View
        val params = view_instance.layoutParams
        params.width = (getWidth() / 100) * 60
        view_instance.layoutParams = params
        val parent = findViewById<View>(R.id.parent_lay) as View
        parent.invalidate()
        parent.requestLayout()
    }

    protected fun getWidth(): Int {
        var displayMetrics: DisplayMetrics = DisplayMetrics()
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels
    }


    fun pushWithAnimation(fragment: Fragment, title: String?) {
        val fragmentManager = supportFragmentManager
        val tag = fragment.javaClass.canonicalName
        if (title != null) {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.apptentive_slide_up_in, R.anim.apptentive_slide_down_out)
                    .replace(R.id.frame_home_screen_container, fragment, tag)
                    .addToBackStack(title)
                    .commit()
        } else {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.apptentive_slide_up_in, R.anim.apptentive_slide_down_out)
                    .replace(R.id.frame_home_screen_container, fragment, tag)
                    .commit()
        }
        if (title != null) {
            val actionBar = supportActionBar
            if (actionBar != null) {
                actionBar.title = title
            }
        }
    }

    fun push(fragment: Fragment?) {
        if (fragment == null) {
            return
        }
        mFirebaseAnalytics.setCurrentScreen(this, fragment.javaClass.simpleName, null)
        push(fragment, null)
    }

    fun pushFront(fragment: Fragment, title: String) {
        val backCount = supportFragmentManager.backStackEntryCount
        if (backCount > 0) {
            BaseFragment.sDisableFragmentAnimations = true
            popAllBackstack()
            push(fragment, title)
            supportFragmentManager.executePendingTransactions()
            BaseFragment.sDisableFragmentAnimations = false
        } else {
            push(fragment, title)
        }
    }

    // error displaying methods
    fun showException(throwable: Throwable) {
        showException(ERROR_SHOW_TYPE_DIALOG, throwable)
    }

    fun showException(showType: Int, throwable: Throwable) {
        Timber.e(throwable, "showException")
        val error = extractError(throwable)
        showError(showType, error,null)
    }

    fun dismissDialogFragment(tag: String) {
        val fragmentManager = supportFragmentManager
        val fragment = fragmentManager.findFragmentByTag(tag) as DialogFragment
        if (fragment != null && fragment.isVisible) {
            fragment.dismissAllowingStateLoss()
        }
    }

    fun showError(showType: Int, error: UserError) {
        showError(showType, error,null)
    }


    fun showError(showType: Int, error: UserError, callback: DialogInterface.OnClickListener?) {
        if(!isFinishing) {
            when (showType) {
                ERROR_SHOW_TYPE_LOG -> Log.e("BaseActivity", "Error title : " + error.title + "; Message : " + error.message)
                ERROR_SHOW_TYPE_TOAST -> if (!TextUtils.isEmpty(error.message)) {
                    Toast.makeText(this, error.message, Toast.LENGTH_LONG).show()
                }
                ERROR_SHOW_TYPE_DIALOG -> {
//                dismissDialogFragment(DIALOG_API_ERROR_TAG)


                    if (alertFragment != null) {
                        alertFragment!!.dismissAllowingStateLoss()
                    }
                    alertFragment = AlertDialogFragment.newOkDialog(error.title, error.message)
                    if (callback != null) {
                        alertFragment!!.setListener(callback)
                    }
                    supportFragmentManager.beginTransaction()
                            .add(alertFragment, DIALOG_API_ERROR_TAG)
                            .commitAllowingStateLoss()

                }
            }
        }

    }

    fun showMessage(message: String?) {
        if (!isFinishing && !TextUtils.isEmpty(message)) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }

    /**
     * Simple error handling by displaying an dialog if the exception
     * is not handled by the base class.
     */
    fun handleError(exception: Throwable) {
        hideProgress()
        showException(exception)
    }

    fun displayShortToast(msg: Int) {
        Toast.makeText(NCCApplication.getContext(), msg, Toast.LENGTH_SHORT).show()
    }

    fun displayLongToast(msg: String) {
        Toast.makeText(NCCApplication.getContext(), msg, Toast.LENGTH_LONG).show()
    }

    companion object {

        val ERROR_SHOW_TYPE_LOG = 0
        val ERROR_SHOW_TYPE_TOAST = 1
        val ERROR_SHOW_TYPE_DIALOG = 2

        val DIALOG_API_ERROR_TAG = "apiTag"


        fun extractError(throwable: Throwable): UserError {
            val context = NCCApplication.getContext()

            var title: CharSequence = context.getString(R.string.error)
            var message: String = context.getString(R.string.application_error_message, throwable.message)
            val errorId: String? = null

            if (BuildConfig.DEBUG) {
                message = message + "\n\nTech Msg: " + throwable.toString()
            }
            return UserError(errorId, title, message)
        }
    }
}// API Call handling
