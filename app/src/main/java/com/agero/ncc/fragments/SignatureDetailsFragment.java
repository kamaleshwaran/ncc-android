package com.agero.ncc.fragments;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.firestore.FirestoreJobData;
import com.agero.ncc.model.Signature;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import durdinapps.rxfirebase2.RxFirebaseStorage;
import me.srodrigo.androidhintspinner.HintSpinner;


public class SignatureDetailsFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {

    @BindView(R.id.spinner_disablement_signature)
    Spinner mSpinnerDisablementSignature;
    @BindView(R.id.edit_disablement_signature)
    EditText mEditDisablementSignature;
    @BindView(R.id.image_signature)
    ImageView mImageSignature;
    @BindView(R.id.text_signature)
    TextView mTextSignature;
    @BindView(R.id.constriant_signature)
    ConstraintLayout mConstriantSignature;
    @BindView(R.id.tv_disablement_signature)
    TextView mTextDisablementSignature;
    boolean isFromDisablementDir;
    HomeActivity mHomeActivity;
    static Bitmap mDisablementBitmap;
    static Bitmap mTowBitmap;
    @BindView(R.id.image_get_signature)
    ImageView mImageGetSignature;
    private boolean isEditable;
    private String mDispatcherId;
    private DatabaseReference mSignatureReference;
    String mImageUrl = "";
    private UserError mUserError;
    private long oldRetryTime = 0;
    boolean isTextChanged;
    String mChangedTextSign;
    HintSpinner<String> hintSpinner;

    private Signature mSignature;

    public SignatureDetailsFragment() {
        // Intentionally empty
    }

    public static SignatureDetailsFragment newInstance(boolean isFromDisablementDir, boolean isEditable, String dispatcherId, boolean isItFromHistory) {
        SignatureDetailsFragment fragment = new SignatureDetailsFragment();
        Bundle args = new Bundle();
        args.putBoolean(NccConstants.IS_IT_FROM_DISABLEMENT_DIRECTION, isFromDisablementDir);
        args.putBoolean(NccConstants.IS_EDITABLE, isEditable);
        args.putBoolean(NccConstants.IS_IT_FROM_HISTORY, isItFromHistory);
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, dispatcherId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_signature_details, fragment_content, true);
        ButterKnife.bind(this, view);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.setOnToolbarSaveListener(this);
//        mConstriantSignature.setEnabled(false);
        mUserError = new UserError();

        mSpinnerDisablementSignature.setVisibility(View.VISIBLE);
        mTextDisablementSignature.setVisibility(View.GONE);
        mEditDisablementSignature.addTextChangedListener(textwatcher);
        if (getArguments() != null) {
            isFromDisablementDir = getArguments().getBoolean(NccConstants.IS_IT_FROM_DISABLEMENT_DIRECTION);
            isEditable = getArguments().getBoolean(NccConstants.IS_EDITABLE);
            mDispatcherId = getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER);
        }
        if (isFromDisablementDir) {
            mHomeActivity.updateAnalyticsCurrentScreen(getResources().getString(R.string.title_disablement_sign));
            mHomeActivity.showToolbar(getString(R.string.title_disablement_sign));
            List<String> mSignDisablementOptions = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.array_disablement_signature_reason)));
            setAdapter(mSignDisablementOptions);
            /*hintSpinner = new HintSpinner<>(
                    mSpinnerDisablementSignature,
                    new HintAdapter<>(getActivity(), R.string.disablement_your_hint_text, mSignOptions),
                    new HintSpinner.Callback<String>() {
                        @Override
                        public void onItemSelected(int position, String itemAtPosition) {
                            enableSave();
                        }
                    });
            hintSpinner.init();*/
        } else {
            mHomeActivity.showToolbar(getString(R.string.title_dropoff_sign));
            mHomeActivity.updateAnalyticsCurrentScreen(getResources().getString(R.string.title_dropoff_sign));
            List<String> mSignOptions = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.array_drop_off_signature_reason)));
            setAdapter(mSignOptions);
           /* hintSpinner = new HintSpinner<>(
                    mSpinnerDisablementSignature,
                    new HintAdapter<>(getActivity(), R.string.disablement_your_hint_text, mSignOptions),
                    new HintSpinner.Callback<String>() {
                        @Override
                        public void onItemSelected(int position, String itemAtPosition) {
                            enableSave();
                        }
                    });
            hintSpinner.init();*/
        }
        if (!isEditable) {
//            mSpinnerDisablementSignature.setEnabled(false);
            mSpinnerDisablementSignature.setVisibility(View.INVISIBLE);
            mTextDisablementSignature.setVisibility(View.VISIBLE);
            mConstriantSignature.setEnabled(false);
            mEditDisablementSignature.setEnabled(false);
            mTextSignature.setEnabled(false);
            mHomeActivity.hideSaveButton();
        }
        if(FirestoreJobData.getInStance().isFirestore()){
            try {
                if (isFromDisablementDir) {
                    mSignature = FirestoreJobData.getInStance().getJobDetail().getDisablementLocation().getSignature();
                } else {
                    mSignature = FirestoreJobData.getInStance().getJobDetail().getTowLocation().getSignature();
                }
                displaySignatureData();
            }catch (Exception e){
                mHomeActivity.mintLogException(e);
            }

            enableSave();
        }else {
            initFirebaseDatabase();
            if (mDisablementBitmap == null && mTowBitmap == null) {
                getSignatureDetails();
            } else {
                showSignature();
            }
        }
        enableSave();
        return superView;
    }

    private void setAdapter(List<String> mSignOptions) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(), R.layout.spinner_dropdown_item, R.id.text_item_list, mSignOptions) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @NonNull
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView textSpinner = (TextView) v.findViewById(R.id.text_item_list);
                String spinnerReasonHint = textSpinner.getText().toString();
                if (spinnerReasonHint.equals("Reason for signature")) {
                    textSpinner.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
                } else {
                    textSpinner.setTextColor(getResources().getColor(R.color.ncc_black));
                }
                return v;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView textReason = (TextView) view.findViewById(R.id.text_item_list);
                if (position == 0) {
                    // Set the hint text color gray
                    textReason.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
                } else {
                    textReason.setTextColor(getResources().getColor(R.color.ncc_black));
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mSpinnerDisablementSignature.setAdapter(spinnerArrayAdapter);
        mSpinnerDisablementSignature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                enableSave();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void showSignature() {
        Drawable drawable;
        if (isFromDisablementDir) {
            drawable = new BitmapDrawable(getResources(), mDisablementBitmap);
        } else {
            drawable = new BitmapDrawable(getResources(), mTowBitmap);
        }
        mTextSignature.setVisibility(View.GONE);
        mImageSignature.setVisibility(View.GONE);
        mImageGetSignature.setVisibility(View.VISIBLE);
        mImageGetSignature.setImageDrawable(drawable);
    }

    TextWatcher textwatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            mChangedTextSign = mEditDisablementSignature.getText().toString();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!mChangedTextSign.equals(s.toString()) && mChangedTextSign.length() > 0) {
                isTextChanged = true;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            enableSave();
        }
    };

    public void enableSave() {
        if (isAdded()) {
            String signeeName = mEditDisablementSignature.getText().toString().trim();

//            String selectedReason = mSpinnerDisablementSignature.getSelectedItem().toString();

            int selectedItemPosition = mSpinnerDisablementSignature.getSelectedItemPosition();

            if (TextUtils.isEmpty(signeeName) ||
                    selectedItemPosition < 0 || isSignatureNotPresent()) {
                mHomeActivity.saveDisable();
            } else if (mSignature != null && (signeeName.equalsIgnoreCase(mSignature.getSigneeName()) && mSignature.getReason().equalsIgnoreCase(isFromDisablementDir ? getResources().getStringArray(R.array.array_disablement_signature_reason)[selectedItemPosition] : getResources().getStringArray(R.array.array_drop_off_signature_reason)[selectedItemPosition])) && (isFromDisablementDir ? mDisablementBitmap == null : mTowBitmap == null)) {
                mHomeActivity.saveDisable();
            } else {
                mHomeActivity.saveEnable();
            }


        }

    }

    private boolean isSignatureNotPresent() {
        if (isFromDisablementDir) {
            return mDisablementBitmap == null && TextUtils.isEmpty(mImageUrl);
        } else {
            return mTowBitmap == null && TextUtils.isEmpty(mImageUrl);
        }
    }


    @OnClick(R.id.constriant_signature)
    public void onViewClicked() {
        if (isEditable) {
            if (isFromDisablementDir) {
                getSignatureFragment();
            } else {
                getSignatureFragment();
            }
        }
    }

    @Override
    public void onSave() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        if ((isFromDisablementDir && mDisablementBitmap != null) || (!isFromDisablementDir && mTowBitmap != null)) {
                            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

                            StringBuilder signaturePathBuilder = new StringBuilder();
                            signaturePathBuilder.append("Documents/");
                            signaturePathBuilder.append(mDispatcherId);
                            signaturePathBuilder.append("/Signature/");


//                            StorageReference storageReference = firebaseStorage.getReference()
//                                    .child("Documents/" + mDispatcherId + "/Signature/" + System.currentTimeMillis()
//                                            + filePath.substring(filePath.lastIndexOf(".")));

                            if (isFromDisablementDir) {
                                signaturePathBuilder.append("disablement-sign.png");
                            } else {
                                signaturePathBuilder.append("drop-off-sign.png");
                            }
                            StorageReference storageReference = firebaseStorage.getReference()
                                    .child(signaturePathBuilder.toString());
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            if (isFromDisablementDir) {
                                mDisablementBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            } else {
                                mTowBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            }
                            byte[] byteArray = stream.toByteArray();
                            RxFirebaseStorage.putBytes(storageReference, byteArray).subscribe(taskSnapshot -> {
                                if (taskSnapshot.getDownloadUrl() != null) {
                                    uploadModelValues(taskSnapshot.getDownloadUrl().toString());

                                }
                            }, throwable -> {
                                hideProgress();
                                if (isAdded()) {
                                    mHomeActivity.mintlogEvent("Signature Details Save Signature Firebase Database Error - " + throwable.getLocalizedMessage());
                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.edit_profile_error)));
                                }
                            });
                        } else {
                            uploadModelValues(mImageUrl);
                        }
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void getSignatureFragment() {
        if (getResources().getBoolean(R.bool.isTablet)) {
            if (isFromDisablementDir) {
                mHomeActivity.push(SignatureTabletFragment.newInstance(true, mImageUrl), getString(R.string.title_disablement_sign));
            } else {
                mHomeActivity.push(SignatureTabletFragment.newInstance(false, mImageUrl), getString(R.string.title_dropoff_sign));
            }
        } else {
            if (isFromDisablementDir) {
                mHomeActivity.push(SignatureFragment.newInstance(true, mImageUrl), getString(R.string.title_disablement_sign));
            } else {
                mHomeActivity.push(SignatureFragment.newInstance(false, mImageUrl), getString(R.string.title_dropoff_sign));
            }
        }
    }

    private void uploadModelValues(String mImageUrl) {
        Signature signature = new Signature();
        signature.setReason(mSpinnerDisablementSignature.getSelectedItem().toString());
        signature.setSigneeName(mEditDisablementSignature.getText().toString());
        signature.setSignatureUrl(mImageUrl);

        HashMap<String, String> extraDatas = new HashMap<>();
        extraDatas.put("dispatch_id", mDispatcherId);
        extraDatas.put("json", new Gson().toJson(signature));
        mHomeActivity.mintlogEventExtraData("Signature Details Signature Data Upload", extraDatas);


        mSignatureReference.setValue(signature).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(isAdded() && isVisible()) {
                    hideProgress();
                    mHomeActivity.onBackPressed();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                hideProgress();
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_TOAST(), new UserError("", "", getString(R.string.error_failed_to_upload_signature)));
            }
        });
    }

    private void initFirebaseDatabase() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        String signaturePath;
        String initUrl = "ActiveJobs/";
        if (getArguments().getBoolean(NccConstants.IS_IT_FROM_HISTORY)) {
            initUrl = "InActiveJobs/";
        }
        if (isFromDisablementDir) {
            signaturePath = initUrl + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                    + "/" + mDispatcherId + "/disablementLocation/signature";
        } else {
            signaturePath = initUrl + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                    + "/" + mDispatcherId + "/towLocation/signature";
        }
        mSignatureReference = firebaseDatabase.getReference(signaturePath);
        //mSignatureReference.keepSynced(true);
    }

    private void getSignatureDetails() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        mSignatureReference.addValueEventListener(signatureValueEventListener);
                    }
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }
    }

    ValueEventListener signatureValueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            hideProgress();
            if (dataSnapshot.hasChildren() && isAdded()) {
                try {
                    mSignature = dataSnapshot.getValue(Signature.class);
                    displaySignatureData();

                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }
            try {
                mSignatureReference.removeEventListener(signatureValueEventListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
            enableSave();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Signature Details Signature Data Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getSignatureDetails();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private void displaySignatureData() {
        try{
            if (mSignature != null) {
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("dispatch_id", mDispatcherId);
                extraDatas.put("json", new Gson().toJson(mSignature));
                mHomeActivity.mintlogEventExtraData("Signature Details Signature Data", extraDatas);
            }

            String[] reasonArray;
            if (isFromDisablementDir) {
                reasonArray = getResources().getStringArray(R.array.array_disablement_signature_reason);
            } else {
                reasonArray = getResources().getStringArray(R.array.array_drop_off_signature_reason);
            }
            mTextDisablementSignature.setText(mSignature.getReason());
            if (reasonArray[1].equals(mSignature.getReason())) {
                mSpinnerDisablementSignature.setSelection(1);
            } else {
                mSpinnerDisablementSignature.setSelection(2);
            }
            mEditDisablementSignature.setText(mSignature.getSigneeName());
            if (mSignature.getSignatureUrl() != null) {
                mTextSignature.setVisibility(View.GONE);
                mImageSignature.setVisibility(View.GONE);
                mImageGetSignature.setVisibility(View.VISIBLE);
                mImageUrl = mSignature.getSignatureUrl();
                Glide.with(mHomeActivity)
                        .load(mSignature.getSignatureUrl())
                        .into(mImageGetSignature);
            }
        }catch (Exception e){
            mHomeActivity.mintLogException(e);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (mSignatureReference != null) {
                mSignatureReference.removeEventListener(signatureValueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (mSignatureReference != null) {
                mSignatureReference.removeEventListener(signatureValueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (mSignatureReference != null) {
                mSignatureReference.removeEventListener(signatureValueEventListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
