package com.agero.ncc.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.utils.NccConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EtaApprovalDialogFragment extends DialogFragment {


    @BindView(R.id.text_eta_approval)
    TextView mTextEtaApproval;
    @BindView(R.id.text_eta_approval_description)
    TextView mTextEtaApprovalDescription;
    @BindView(R.id.radio_traffic)
    RadioButton mRadioTraffic;
    @BindView(R.id.radio_weather)
    RadioButton mRadioWeather;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;
    @BindView(R.id.text_ok)
    TextView mTextOk;
    @BindView(R.id.text_cancel)
    TextView mTextCancel;
    @BindView(R.id.radio_reason_not_listed)
    RadioButton mRadioReasonNotListed;

    private int mSelectedPosition;

    public EtaApprovalDialogFragment() {

    }

    public static EtaApprovalDialogFragment newInstance() {
        EtaApprovalDialogFragment fragment = new EtaApprovalDialogFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_eta_approval, container);
        ButterKnife.bind(this, v);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mTextOk.setEnabled(false);
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                mTextOk.setEnabled(true);
                mTextOk.setTextColor(getResources().getColor(R.color.jobdetail_background_button));
                switch (radioGroup.getCheckedRadioButtonId()){
                    case R.id.radio_traffic:
                        mSelectedPosition = 0;
                        break;
                    case R.id.radio_weather:
                        mSelectedPosition = 1;
                        break;
                    case R.id.radio_reason_not_listed:
                        mSelectedPosition = 2;
                        break;
                }
            }
        });
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @OnClick({R.id.text_ok, R.id.text_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.text_ok:
                if(getTargetFragment() != null && !getTargetFragment().isDetached()) {
                    Intent intent = new Intent();
                    intent.putExtra("position",mSelectedPosition);
                    getTargetFragment().onActivityResult(NccConstants.DISPATCHER_REQUEST_JOB, Activity.RESULT_OK, intent);
                }
                dismiss();
                break;
            case R.id.text_cancel:
                dismiss();
                break;
        }
    }

}
