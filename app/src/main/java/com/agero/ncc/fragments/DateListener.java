package com.agero.ncc.fragments;

public interface DateListener {
    void onDateSet(java.util.Date date);
    void onReset();
}
