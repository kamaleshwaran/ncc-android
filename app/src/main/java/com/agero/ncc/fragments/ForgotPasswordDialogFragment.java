package com.agero.ncc.fragments;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.R;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.utils.NccConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.agero.ncc.fragments.BaseFragment.isValidEmail;
import static com.agero.nccsdk.NccSdk.getApplicationContext;


public class ForgotPasswordDialogFragment extends DialogFragment {

    @BindView(R.id.text_label_forget_password)
    TextView mTextLabelForgetPassword;
    @BindView(R.id.text_label_enter_email)
    TextView mTextLabelEnterEmail;
    @BindView(R.id.edit_forget_email_password)
    EditText mEditForgetEmailPassword;
    @BindView(R.id.button_reset)
    TextView mButtonReset;
    @BindView(R.id.button_cancel)
    TextView mButtonCancel;
    private OnResetClick resetClick;
    String mEmail;
    WelcomeActivity mWelcomeActivity;
    public ForgotPasswordDialogFragment() {
        // Intentionally empty
    }

    public void setOnResetClick(OnResetClick resetClick) {
        this.resetClick = resetClick;
    }
    public static ForgotPasswordDialogFragment newInstance(String userEmail) {
        ForgotPasswordDialogFragment fragment = new ForgotPasswordDialogFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.USER_EMAIL, userEmail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_forget_password, container, false);
        ButterKnife.bind(this, rootView);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mEditForgetEmailPassword.addTextChangedListener(mTextWatcher);
        if(getArguments()!=null){
            mEmail = getArguments().getString(NccConstants.USER_EMAIL,"");
        }
        if(!mEmail.isEmpty()){
            mEditForgetEmailPassword.setText(mEmail);
        }else{
            mEditForgetEmailPassword.setText("");
        }
        return rootView;
    }
    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.button_reset, R.id.button_cancel})
    public void onViewClicked(View view) {
//        String mSubject = getResources().getString(R.string.reset_password);
//        String mToEmail = BuildConfig.SUPPORT_MAIL_ID;
//        String mUniqueId = mPrefs.getString(NccConstants.DEVICE_ID,"");


//        String mAppName = getResources().getString(R.string.label_app_name) +" "+ getResources().getString(R.string.app_name);
//        String mEmailId = getResources().getString(R.string.label_email) +" "+mEditForgetEmailPassword.getText().toString();
//        String os = "Android";
//        String mOsVersion = getResources().getString(R.string.label_os_version) +" "+Build.VERSION.RELEASE;
//        String mDeviceId = getResources().getString(R.string.label_device_id) +" "+ mUniqueId;
//        String mAppVersion = getResources().getString(R.string.label_app_version)+" "+ BuildConfig.VERSION_NAME ;


//        String mEmailBody = mAppName +", \n"+mAppVersion+", \n"+mEmailId+", \n"
//                + os + ", \n" + mOsVersion+", \n"+ mDeviceId;

        switch (view.getId()) {
            case R.id.button_reset:
//                Intent sendMail = new Intent(Intent.ACTION_VIEW);
//                sendMail.setData(Uri.parse("mailto:"+mToEmail));
//                sendMail.putExtra(Intent.EXTRA_SUBJECT, mSubject);
//                sendMail.putExtra(Intent.EXTRA_TEXT, mEmailBody);
//                startActivity(sendMail);
//                PasswordFragment.isEmailSent=true;
                resetClick.onResetClicked(mEditForgetEmailPassword.getText().toString());
                getDialog().dismiss();
                break;
            case R.id.button_cancel:
                getDialog().dismiss();
                break;
        }
    }

    private  final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(isValidEmail(mEditForgetEmailPassword.getText().toString().trim())){
                mButtonReset.setEnabled(true);
                mButtonReset.setTextColor(getResources().getColor(R.color.ncc_blue));
            }else {
                mButtonReset.setEnabled(false);
                mButtonReset.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
            }
        }
    };

    public interface OnResetClick{
        void onResetClicked(String email);
    }
}
