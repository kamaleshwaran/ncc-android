package com.agero.ncc.fragments;


import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.firebasefunctions.FirebaseFunctions;
import com.agero.ncc.firebasefunctions.FirebaseFunctionsException;
import com.agero.ncc.firebasefunctions.HttpsCallableResult;
import com.agero.ncc.model.ETA;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import timber.log.Timber;

import static com.agero.nccsdk.NccSdk.getApplicationContext;

public class UpdateETAFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {
    HomeActivity mHomeActivity;
    @BindView(R.id.text_added_eta)
    TextView textAddedEta;
    @BindView(R.id.image_eta_decrease)
    ImageView imageEtaDecrease;
    @BindView(R.id.image_eta_increase)
    ImageView imageEtaIncrease;
    @BindView(R.id.text_new_eta)
    TextView textNewEta;
    @BindView(R.id.radio_traffic)
    RadioButton radioTraffic;
    @BindView(R.id.radio_weather)
    RadioButton radioWeather;
    @BindView(R.id.radio_reason_not_listed)
    RadioButton radioReasonNotListed;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;
    @BindView(R.id.edit_reason_not_listed)
    EditText editReasonNotListed;
    private String mDispatcherId = "", mUserName, mTimeZone;
    private String reasonCode = "";

    UserError mUserError;
    private DatabaseReference mJobReference;
    private long oldRetryTime = 0;
    private int mAddedETA = 0;
    private Date mEtaTime;
    private JobDetail mCurrentJob;


    public UpdateETAFragment() {
    }

    public static UpdateETAFragment newInstance(String userName, String mDispatcherId, String timeZone) {
        UpdateETAFragment fragment = new UpdateETAFragment();
        Bundle args = new Bundle();
        args.putString(NccConstants.USERNAME, userName);
        args.putString(NccConstants.DISPATCH_REQUEST_NUMBER, mDispatcherId);
        args.putString(NccConstants.TIME_ZONE, timeZone);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_update_eta, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.showToolbar(getString(R.string.alert_label_update_eta));
        mHomeActivity.setOnToolbarSaveListener(this);
        mHomeActivity.hideBottomBar();
        mHomeActivity.saveDisable();
        saveEnableDisable();
        mUserError = new UserError();

        if (getArguments() != null) {
            mUserName = getArguments().getString(NccConstants.USERNAME, "");
            mDispatcherId = getArguments().getString(NccConstants.DISPATCH_REQUEST_NUMBER, "");
            mTimeZone = getArguments().getString(NccConstants.TIME_ZONE, "");
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        mJobReference = database.getReference("ActiveJobs/"
                + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mDispatcherId);
        //mJobReference.keepSynced(true);
        setETAValue(mAddedETA);
        getETADateTimeFirebase();
        radioReasonNotListed.setTag("RNL");
        radioTraffic.setTag("TFC");
        radioWeather.setTag("WTR");
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                editReasonNotListed.setText("");
                reasonCode = (String) (view.findViewById(radioGroup.getCheckedRadioButtonId())).getTag();
                if (radioGroup.getCheckedRadioButtonId() == R.id.radio_reason_not_listed) {
                    editReasonNotListed.setEnabled(true);
                } else {
                    editReasonNotListed.setEnabled(false);
                }
                saveEnableDisable();
            }
        });

        editReasonNotListed.setEnabled(false);
        editReasonNotListed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                saveEnableDisable();
            }
        });

        editReasonNotListed.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        return superView;
    }


    private void saveEnableDisable() {
        mHomeActivity.saveDisable();
        if (mAddedETA > 0 && (radioGroup.getCheckedRadioButtonId() != -1)) {
            if (radioGroup.getCheckedRadioButtonId() == R.id.radio_reason_not_listed) {
                if (editReasonNotListed.getText().toString().trim().length() > 0) {
                    mHomeActivity.saveEnable();
                }
            } else {
                mHomeActivity.saveEnable();
            }
        }


    }

    private void getETADateTimeFirebase() {
        if (Utils.isNetworkAvailable()) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    if (isAdded()) {
                        RxFirebaseDatabase.observeValueEvent(mJobReference, JobDetail.class).subscribe(taskSnapshot -> {
                            if (isAdded()) {
                                if (taskSnapshot != null) {
                                    mCurrentJob = taskSnapshot;
                                    HashMap<String, String> extraDatas = new HashMap<>();
                                    extraDatas.put("json", new Gson().toJson(taskSnapshot));
                                    mHomeActivity.mintlogEventExtraData("Update Eta Job Details", extraDatas);

                                    String etaDateTime = taskSnapshot.getEtaDateTime();
                                    if (TextUtils.isEmpty(etaDateTime)) {
                                        Calendar mCurrentTime = Calendar.getInstance();
                                        Date date = DateTimeUtils.parse(taskSnapshot.getAcceptedDateTime());
                                        mCurrentTime.setTime(date);
                                        if (taskSnapshot.getEtaExtensionCount() == null || taskSnapshot.getEtaExtensionCount() == 0) {
                                            mCurrentTime.add(Calendar.MINUTE, taskSnapshot.getQuotedEta());
                                        } else {
                                            mCurrentTime.add(Calendar.MINUTE, taskSnapshot.getTotalEtaInMinutes());
                                        }
                                        mEtaTime = mCurrentTime.getTime();

                                    } else {
                                        mEtaTime = DateTimeUtils.parse(etaDateTime);
                                    }
                                    updateEtaTime(mEtaTime);

                                }
                                hideProgress();
                            }

                        }, throwable -> {
                            if (isAdded()) {
                                mHomeActivity.mintlogEvent("Update Eta Job Details Firebase Database Error - " + throwable.getLocalizedMessage());
                                hideProgress();
                                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                                    getETADateTimeFirebase();
                                    oldRetryTime = System.currentTimeMillis();
                                }
                            }
                        });

                    }
                }


                @Override
                public void onRefreshFailure() {
                    if (isAdded()) {
                        hideProgress();
                        mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void updateEtaTime(Date d) {
        if (d != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(d.getTime());
            long currentTimeMillis = calendar.getTimeInMillis() + (mAddedETA * 60 * 1000);
            calendar.setTimeInMillis(currentTimeMillis);
            textNewEta.setText("New ETA " + DateTimeUtils.getDisplayTime(calendar.getTime(), mTimeZone));
        }
    }

    private void setValueToServer() {

        if(mCurrentJob != null) {
            if (Utils.isNetworkAvailable()) {
                showProgress();
                TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                    @Override
                    public void onRefreshSuccess() {
                        if (isAdded()) {
                        /*mJobReference.runTransaction(new Transaction.Handler() {
                            @Override
                            public Transaction.Result doTransaction(MutableData mutableData) {
                                JobDetail jobDetail = mutableData.getValue(JobDetail.class);
                                if (!mHomeActivity.isLoggedInDriver() || jobDetail.getEtaExtensionCount() == null || jobDetail.getEtaExtensionCount() == 0) {
                                    ArrayList<ETA> etaArrayList = jobDetail.getEtaChangeHistory();
                                    if (etaArrayList == null) {
                                        etaArrayList = new ArrayList<>();
                                    }
                                    ETA eta = new ETA("", mPrefs.getString(NccConstants.DEVICE_ID,""), getString(R.string.app_name), mAddedETA, false, reasonCode, editReasonNotListed.getText().toString().trim(), DateTimeUtils.getUtcTimeFormat(), "FIREBASE", NccConstants.SUB_SOURCE, "ETA_PARTNER_APPROVAL_PENDING", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), mUserName);

                                    etaArrayList.add(eta);
                                    jobDetail.setEtaChangeHistory(etaArrayList);
                                    jobDetail.setEtaExtensionCount(jobDetail.getEtaExtensionCount() + 1);
                                    jobDetail.setEtaStatus("REQUEST");
                                    //jobDetail.setTotalEtaInMinute(jobDetail.getQuotedEta() + mAddedETA);


                                    HashMap<String, String> extraDatas = new HashMap<>();
                                    extraDatas.put("json", new Gson().toJson(eta));
                                    extraDatas.put("dispatch_id", mDispatcherId);
                                    mHomeActivity.mintlogEventExtraData("ETA Update", extraDatas);

                                    mutableData.setValue(jobDetail);
                                    mEditor.putBoolean(NccConstants.SHOW_UPDATE_ETA_BAR, true).commit();
                                    return Transaction.success(mutableData);
                                } else {
                                    return Transaction.abort();
                                }
                            }

                            @Override
                            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {
                                hideProgress();
                                mHomeActivity.onBackPressed();
                            }
                        });*/

                        if(mCurrentJob == null){
                            return;
                        }


                            hideProgress();
                            HashMap<String, Object> etaObj = new HashMap<String, Object>();
                            int quotedEta = 0;
                            if (mCurrentJob.getQuotedEta() != null) {
                                quotedEta = mCurrentJob.getQuotedEta();
                            }


                            etaObj.put("totalEtaInMinutes", quotedEta + mAddedETA);
                            etaObj.put("etaStatus", "REQUEST");
                            etaObj.put("etaDateTime", DateTimeUtils.addMinutesToServerTime(mCurrentJob.getEtaDateTime(), mAddedETA));
                            etaObj.put("etaInMinutes", mAddedETA);
                            etaObj.put("reasonCode", reasonCode);
                            etaObj.put("reasonText", editReasonNotListed.getText().toString().trim());
                            etaObj.put("dispatchId", mDispatcherId);
                            etaObj.put("source", NccConstants.SOURCE);
                            etaObj.put("subSource", NccConstants.SUB_SOURCE);
                            etaObj.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                            etaObj.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));


                            HashMap<String, String> extraDatas = new HashMap<>();
                            extraDatas.put("json", new Gson().toJson(etaObj));
                            extraDatas.put("dispatch_id", mDispatcherId);
                            mHomeActivity.mintlogEventExtraData("ETA Update", extraDatas);

                            showProgress();
                            FirebaseFunctions.getInstance()
                                    .getHttpsCallable(NccConstants.API_JOB_ETA_UPDATE)
                                    .call(etaObj)
                                    .addOnFailureListener(new OnFailureListener() {

                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            if (isAdded()) {
                                                hideProgress();
                                                Timber.d("failed");
                                                e.printStackTrace();
                                                UserError mUserError = new UserError();
                                                if (e instanceof FirebaseFunctionsException) {
                                                    FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                                    FirebaseFunctionsException.Code code = ffe.getCode();
                                                    Object details = ffe.getDetails();
//                                        Toast.makeText(((BaseActivity) getActivity()), ffe.getMessage(), Toast.LENGTH_LONG).show();
                                                    mHomeActivity.mintlogEvent("ETA Update: " + " Firebase Functions Error - Error Message -" + ffe.getMessage());
                                                        mUserError.message = ffe.getMessage();
                                                        try {
                                                            JSONObject errorObj = new JSONObject(ffe.getMessage());
                                                            mUserError.title = errorObj.optString("title");
                                                            mUserError.message = errorObj.optString("body");
                                                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                                        } catch (Exception parseException) {
                                                            parseException.printStackTrace();
                                                            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                        }
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);

                                                } else if (e.getMessage() != null) {
                                                    if(getString(R.string.firebase_logout_error_msg).equalsIgnoreCase(e.getMessage())){
                                                        ((HomeActivity) getActivity()).tokenRefreshFailed();
                                                    }else {
                                                        mUserError.message = e.getMessage();
                                                        showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                        mHomeActivity.mintlogEvent("ETA Update: " + " Firebase Functions Error - Error Message -" + e.getMessage());
                                                    }
                                                } else {
                                                    mHomeActivity.mintlogEvent("ETA Update Firebase Functions Error - Error Message -  failed");
                                                    mUserError.message = "failed";
                                                    showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                                                }
                                            }
                                        }
                                    })
                                    .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                        @Override
                                        public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                            if (isAdded()) {
                                                Timber.d("Successful");
                                                hideProgress();
                                                mEditor.putBoolean(NccConstants.SHOW_UPDATE_ETA_BAR, true).commit();
                                                mHomeActivity.onBackPressed();
                                            }
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onRefreshFailure() {
                        hideProgress();
                        if (isAdded()) {
                        mHomeActivity.tokenRefreshFailed();
                        }
                    }
                });

            } else {
                mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
            }
        }

    }

    @OnClick({
            R.id.image_eta_increase, R.id.image_eta_decrease})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.image_eta_increase:
                mAddedETA = mAddedETA + 5;
                setETAValue(mAddedETA);
                break;
            case R.id.image_eta_decrease:
                mAddedETA = mAddedETA - 5;
                setETAValue(mAddedETA);
                break;
        }
    }

    private void setETAValue(int mAddedETA) {
        imageEtaIncrease.setEnabled(true);
        imageEtaDecrease.setEnabled(true);
        imageEtaIncrease.setImageResource(R.drawable.ic_job_detail_add);
        imageEtaDecrease.setImageResource(R.drawable.ic_job_detail_minus);
        if (mAddedETA == 15) {
            imageEtaIncrease.setImageResource(R.drawable.ic_job_detail_add_disable_blue);
            imageEtaIncrease.setEnabled(false);
        } else if (mAddedETA == 0) {
            imageEtaDecrease.setImageResource(R.drawable.ic_job_detail_minus_disable_blue);
            imageEtaDecrease.setEnabled(false);
        }

        textAddedEta.setText(mAddedETA + " min");
        updateEtaTime(mEtaTime);
        saveEnableDisable();
    }

    @Override
    public void onSave() {

        setValueToServer();
    }


}
