package com.agero.ncc.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.db.database.NccDatabase;
import com.agero.ncc.db.entity.Alert;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.views.AlertDetailBottomSheetDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;



/**
 * To Show Alert Detail
 */

public class AlertDetailFragment extends BaseFragment implements HomeActivity.AlertDetailToolbarLisener{

    HomeActivity mHomeActivity;
    Alert alertResult;
    @BindView(R.id.text_service_desc)
    TextView mTextServiceDesc;
    @BindView(R.id.view_service_border)
    View mViewServiceBorder;
    @BindView(R.id.text_alert_time)
    TextView mTextAlertTime;
    @BindView(R.id.text_alert_reason)
    TextView mTextAlertReason;
    @BindView(R.id.text_alert_desc)
    TextView mTextAlertDesc;
    @Inject
    NccDatabase nccDatabase;

    public AlertDetailFragment() {
        // Intentionally empty
    }

    public static AlertDetailFragment newInstance(long alertResultJson) {
        AlertDetailFragment fragment = new AlertDetailFragment();
        Bundle args = new Bundle();
        args.putLong(NccConstants.BUNDLE_ALERT_RESULT, alertResultJson);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_alertdetail, fragment_content, true);
        NCCApplication.getContext().getComponent().inject(this);
        ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar("Job #",getString(R.string.title_alertdetail));
        mHomeActivity.setOnAlertDetailToolbarLisener(this);
        if (getArguments() != null) {
//
            alertResult = nccDatabase.getAlertDao().findByAlertId(getArguments().getLong(NccConstants.BUNDLE_ALERT_RESULT));
            mTextAlertReason.setText("Reason: " + alertResult.getAlertTitle());
            mTextAlertDesc.setText(alertResult.getAlertDescription());
            mTextAlertTime.setText(DateTimeUtils.getDisplayTimeForAlertDetail(alertResult.getUpdatedTime(),null));
        }
        return superView;
    }


    @Override
    public void onSettings() {
        AlertDetailBottomSheetDialog alertDetailBottomSheetDialog = AlertDetailBottomSheetDialog.getInstance();
        alertDetailBottomSheetDialog.show(getFragmentManager(), "Custom Bottom Sheet");
        alertDetailBottomSheetDialog.setAlertDialogClickListener(new AlertDetailBottomSheetDialog.AlertProfileDialogClickListener() {
            @Override
            public void onJobDetailClick() {

            }

            @Override
            public void onContactDispatcherClick() {

            }

            @Override
            public void onDeleteMessageClick() {

            }
        });
    }
}
